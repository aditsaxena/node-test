var express = require('express'),
    mongoose = require('mongoose');

var app     = express();

// var schema = mongoose.Schema,
//     objectId = schema.ObjectId;

var Cat = mongoose.model('Cat', { name: String });

mongoose.connect('mongodb://localhost/test');

app.get('/hello.txt', function(req, res){
  
  // var kitty = new Cat({ name: 'GARf' });
  // kitty.save(function (err) {
  //   if (err) return handleError(err);
  //   console.log('meow');
  // });
  
  // Cat.findOne({ name: /^Zild/ }, function(err, cat){
  //   console.log("Found kitten: " + cat.name);
  // });
  // Cat.find({ name: /^Zild/ }, callback);
  
  Cat.find({}, function(err, cats){
    if (err) return handleError(err);
    
    var fin_arr = [];
    
    for (var icat in cats) {
      cat = cats[icat];
      fin_arr.push({
        "name": cat.name,
        "desc": 'Hell of a cat'
      });
    };
    
    res.send(fin_arr);
  });
  
  // Person
  // .find({ occupation: /host/ })
  // .where('name.last').equals('Ghost')
  // .where('age').gt(17).lt(66)
  // .where('likes').in(['vaporizing', 'talking'])
  // .limit(10)
  // .sort('-occupation')
  // .select('name occupation')
  // .exec(callback);

  // .update({a:1}, {b:1}, {upsert:true}, function(err, result) {
  //   console.log('cc');
  // });
  
  // db.books.insert({ name:'Palletz', description: "This cat is from Orvieto"})
});

app.use(function(err, req, res, next){
  console.error(err.stack);
  res.send(500, 'Something broke!');
});

app.listen(3002);
console.log('Listening on port 3002');
